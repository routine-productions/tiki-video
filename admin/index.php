<?php

$host = 'localhost';
$database = 'tiki-video';
$user = 'root';
$pswd = '987975';

error_reporting(E_ALL);

$dsn = "mysql:host=$host;dbname=$database;charset=utf8";
$opt = array(
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
);



try {
    $pdo = new PDO($dsn, $user, $pswd, $opt);
} catch (PDOException $e) {
    die('Подключение не удалось: ' . $e->getMessage());
}

$pdo->query('SET NAMES utf8');



$URI = explode('?', $_SERVER['REQUEST_URI']);
$URI = preg_split('@/@', $URI[0], NULL, PREG_SPLIT_NO_EMPTY);


if (method_exists('Actions', $URI[1])) {
    $Actions = new Actions($URI,$pdo);
    $Actions->$URI[1]();
} else {
    $Actions = new Actions($URI,$pdo);
    $Actions->index();
}

class Actions
{
    public $Uri;
    public $PBO;

    public function __construct($URI,$PDO){
        $this->Uri = $URI;
        $this->PDO = $PDO;
    }

    public function index()
    {
        ?>
        <!DOCTYPE html>
        <html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../css/admin_style.css"/>
        <link href='https://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic'
            rel='stylesheet'
            type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic'
            rel='stylesheet'
            type='text/css'>
    </head>
    <body>

    <div class="Head">
        <div class="Head-Content">
            <img src="../img/admin-logo.png" alt=""/>

            <p>Реестр видеофайлов на сайте TiKi-Video</p>

            <div class="Button">
                <div class="Add">

                </div>
            </div>
        </div>
    </div>

    <div class="Video-Wrapper">
        <div class="Videos">
            <div class="Videos-Head">
                <div class='Col c-1'>№</div>
                <div class='Col c-2'>НАЗВАНИЕ</div>
                <div class='Col c-3'>ОПИСАНИЕ</div>
                <div class='Col c-4'>ИЗОБРАЖЕНИЕ</div>
                <div class='Col c-5'>КОД YOUTUBE</div>
                <div class='Col c-6'>СОРТИРОВКА</div>
                <div class='Col col-last'>УПРАВЛЕНИЕ</div>

            </div>
            <?php
            error_reporting(1);
            $i=1;
            $stmt = $this->PDO->query('SELECT * FROM `video`');
            while ($row = $stmt->fetch())
            {
                echo "<div class='One-Row'>";
                echo "<div class='Col c-1' data-id=" . $row['id'] . ">" . $i  . "</div>". "\n";
                echo "<div class='Col c-2' contenteditable='true'>" . $row['name'] . "\n" . "</div>";
                echo "<div class='Col c-3' contenteditable='true'>" . $row['description'] . "\n" . "</div>";
                echo "<div class='Col c-4' contenteditable='true'>" . "<img src=" . "../img/" . $row['image'] . ">" . "\n" . "</div>";
                echo "<div class='Col c-5' contenteditable='true'>" .htmlspecialchars($row['iframe']) . "\n" . "</div>";
                echo "<div class='Col c-6' contenteditable='true'>" . $row['sort'] . "\n" . "</div>";
                echo "<div class='Col c-7'>" . "<div class='Edit'></div>" . "\n" . "</div>";
                echo "<div class='Col c-8'>" . "<div class='Delete'></div>" . "\n" . "</div>";
                echo "</div>";
                $i++;
            }
            ?>
        </div>
    </div>


    <div class="Head Bottom">
        <div class="Head-Content">
            <img src="../img/admin-logo.png" alt=""/>

            <p>Реестр видеофайлов на сайте TiKi-Video</p>

            <div class="Button">
                <div class="Add">

                </div>
            </div>
        </div>
    </div>


    </body>
        </html><?php
    }

    public function remove()
    {
        if(isset($this->Uri[2])){
            $query = "DELETE FROM `video` WHERE `id` = {$this->Uri[2]}";
            $this->PDO->query($query);
        }
    }

    public function add()
    {
        echo 3;
    }
}


mysql_close($dbh);

?>









