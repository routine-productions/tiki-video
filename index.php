<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="./css/index.css"/>
    <link rel="stylesheet" href="./css/media.css"/>
    <link rel="stylesheet" href="./css/fonts.css"/>
    <link rel="stylesheet" href="js/plugins/window/jquery.window.css"/>
    <link rel="stylesheet" href="js/plugins/placeholder/placeholder.css"/>
    <link rel="stylesheet" href="js/plugins/mailForm/mailForm.css"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>


    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="./js/animation.js"></script>
    <script src="/js/plugins/placeholder/placeholder.js"></script>
    <script src="/js/plugins/jquery.actual.min.js"></script>
    <script src="/js/plugins/window/jquery.window.js"></script>
    <script src="/js/plugins/mailForm/jquery.upf.mailForm.js"></script>
</head>
<body>
<div class="Menu-Top">
    <div class="Menu-Top-In">
        <div class="Email-Top">
            <a href="mailto:tiki-video@gmail.com">tiki-video@gmail.com</a>
        </div>

        <div class="Header-1-Text">
            <span>Главная</span>
            <span>Наши услуги</span>
            <span>Наши работы</span>
            <span>О нас</span>
            <span>Контакты</span>
        </div>

        <div class="Language">
            <a href="">UA</a>
            <a href="">RU</a>
            <a href="">EN</a>
        </div>
    </div>
</div>
<div class="Page-1 Parent">

    <div class="Header-1">
        <div class="Logo-Wrapp">
            <img class="Logo-Head" src="img/logo.png" alt=""/>
            <img class="Wheel-Small" src="img/Weel-Small.png" alt=""/>
            <img class="Wheel-Big" src="img/Weel-Big.png" alt=""/>
        </div>


        <div class="Phone-Top">
            <div class="Professional">
                <h3>Профессиональная</h3>

                <h3>съемка музыкальных видео</h3>
            </div>
            <h2>+38 (097) 223-61-16</h2>

            <h2>+38 (095) 486-03-90</h2>

        </div>
    </div>

    <div class="Central">

        <video id="video_background" preload="auto" autoplay="true" loop="loop">
            <source src="./video/Urban_FM.mp4" type='video/mp4'>
            <source src="./video/Urban_FM.ogv" type='video/ogg'>
            <source src="./video/Urban_FM.webm" type='video/webm'>
        </video>
        <div class="Blond-Back"></div>

    </div>

    <div class="Bottom">


        <div class="Button">Наши услуги</div>
        <div class="qwe qwe-3"></div>
    </div>
</div>
<div class="Page-2 Parent">
    <div class="Top">
        <h2>Наши услуги</h2>
        <hr/>
        <p>Мы предлагаем выбрать тот тип видеосъемки, который подойдет именно Вам
        </p>
    </div>
    <div class="Central">
        <div class="Item-Holder">
            <div class="Item-Holder-In">
                <div class="Item-Holder-Show">
                    <img class="Im-1" src="./img/P2-video.png" alt=""/>

                    <h3>Постановочный<br/> видеоклип</h3>


                    <div class="HR2"></div>
                    <p>Сюжетное видео с Вашим участием.</p>
                </div>
                <div class="Item-Holder-Hide">
                    <p>Прекрасно подойдет для формата ТВ, а также для размещения на интернет каналах </p>
                    <ul>
                        <li> написание сценария</li>
                        <li> подбор локации</li>
                        <li> подбор актеров</li>
                        <li> разработка режиссерского решения, согласно вашей идее</li>
                        <li> видеосъемка</li>
                        <li> монтаж материала</li>
                        <li> пост-продакшн</li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="Item-Holder">
            <div class="Item-Holder-In">
                <div class="Item-Holder-Show">
                    <img class="Im-2" src="./img/P2-video-2.png" alt=""/>

                    <h3>Концертный <br/>видеоклип</h3>


                    <div class="HR2 HR3"></div>
                    <p>Live запись Вашего "живого" выступления.</p>
                </div>
                <div class="Item-Holder-Hide">

                    <ul>
                        <li> видеосъемка на 3 и более камеры</li>
                        <li> монтаж материала</li>
                        <li> пост-продакшн</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="Item-Holder">
            <div class="Item-Holder-In">
                <div class="Item-Holder-Show">
                    <img class="Im-3" src="./img/P2-rehearsal.png" alt=""/>

                    <h3>Репетиционное <br/> видео</h3>

                    <div class="HR2"></div>
                    <p>Запись видео на репетиционной базе.</p>
                </div>
                <div class="Item-Holder-Hide">
                    <p>Видеосъемка вашей репетиции это отличный контент для вашего сайта и страниц в социальных
                        сетях.</p>
                    <ul>
                        <li> подбор репетиционной базы</li>
                        <li> видеосъемка</li>
                        <li> монтаж отснятого материала</li>
                        <li> пост-продакшн</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="Item-Holder">
            <div class="Item-Holder-In">
                <div class="Item-Holder-Show">
                    <img class="Im-4" src="./img/P2-visitka.png" alt=""/>

                    <h3>Видео-визитка</h3>

                    <div class="HR2"></div>
                    <p>Создание видео с контактными данными Вашего коллектива.</p>
                </div>
                <div class="Item-Holder-Hide">
                    <p>Промо-ролик для самопрезентации с вашими контактами, также хорошо подходит для рекламы альбома и
                        предстоящего выступления.</p>
                    <ul>
                        <li> подбор локации</li>
                        <li> видеосъемка</li>
                        <li> монтаж материала</li>
                        <li> пост-продакшн</li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="Bottom">
        <div class="Button">Наши работы</div>
        <div class="qwe"></div>
    </div>
</div>


<div class="Page-3 Parent">
    <div class="Top">
        <h2>Наши работы</h2>
        <hr/>
        <p> Мы разрабатываем оптимальное режиссерское решение и план съемки, чтобы в максимально короткие сроки сделать
            качественную работу.
            Также наши видео проходят процесс пост-обработки для обеспечения максимального качества.
        </p>
    </div>
    <div class="Central">

        <div class="Video-Container-Wrapp">
            <div class="Video-Container-Wrapp-2"
                 data-window-youtube="https:/www.youtube.com/embed/ZNUm6vGZYMg?autoplay=1&showinfo=0">
                <div class="Video-Container">
                    <div class="Img-Container Cont-Number-4">
                        <div class="Play-Button">
                        </div>
                    </div>
                    <div class="Text-Container">
                        <h3>Urban FM</h3>
                        <hr/>
                        <p>Студийный клип группы Urban FM</p>
                    </div>
                </div>
            </div>
            <div class="Video-Container-Wrapp-2"
                 data-window-youtube="https://www.youtube.com/embed/hraAzRnvpOU?autoplay=1&showinfo=0">
                <div class="Video-Container">
                    <div class="Img-Container Cont-Number-1">
                        <div class="Play-Button">
                        </div>

                    </div>
                    <div class="Text-Container">
                        <h3>TV Dangers</h3>
                        <hr/>
                        <p>Видео-визитка группы TV Dangers.</p>
                    </div>
                </div>
            </div>

            <div class="Video-Container-Wrapp-2"
                 data-window-youtube="https://www.youtube.com/embed/1q7WJexa370?autoplay=1&showinfo=0">
                <div class="Video-Container">
                    <div class="Img-Container Cont-Number-2">
                        <div class="Play-Button">
                        </div>
                    </div>
                    <div class="Text-Container">
                        <h3>Endlesshade</h3>
                        <hr/>
                        <p>Концертный клип группы Endlesshade, клуб «Bingo».</p>
                    </div>
                </div>

            </div>
            <div class="Video-Container-Wrapp-2"
                 data-window-youtube="https://www.youtube.com/embed/SjcHa-UD3lk?autoplay=1&showinfo=0">
                <div class="Video-Container">
                    <div class="Img-Container Cont-Number-3">
                        <div class="Play-Button">
                        </div>
                    </div>
                    <div class="Text-Container">
                        <h3>Odradek Room</h3>
                        <hr/>
                        <p>Концертный клип группы Odradek Room, клуб «Bingo».</p>
                    </div>
                </div>
            </div>


        </div>

    </div>
    <div class="Bottom">
        <div class="Button">О нас</div>
        <div class="qwe-2"></div>
    </div>
</div>
<div class="Page-4 Parent">
    <img src="../img/logo_by_we.png" alt="" class="Logo-By-We">
    <div class="Top">
        <h2>О нас</h2>
        <hr/>

        <p>Команда, состоящая из профессионалов в области режиссуры, видеосъемки, монтажа и по
            совместительству музыкантов работает над тем,чтобы вывести украинские группы на более высокий уровень.</p>

        <p> Существует множество талантливых исполнителей, заслуживающих внимания,
            у которых есть записанные альбомы, но мало качественных клипов.
            Наша задача – помочь музыкантам презентовать свое творчество и стать
            более известными.</p>

        <p>
            Работы студии tiki-video размещаются в социальных сетях, а также на интернет-каналах.
            Мы хотим показать всем, что украинские коллективы ничем не хуже западных коллег,
            а в чем-то даже лучше. Давайте поддержим наших исполнителей вместе!
        </p>


    </div>
    <div class="Bottom">
        <div class="Button">Контакты</div>
        <div class="qwe"></div>
    </div>

</div>
<div class="Page-5">
    <div class="Top">
        <h2>Свяжитесь с нами</h2>
        <hr/>
        <p>По вопросам и предложениям сотрудничества обращайтесь по указанным ниже контактам. Ответим всем.
        </p>
    </div>
    <div class="Central">
        <div class="Contact-Box-1">
            <h3>Контакты</h3>

            <div class="Email">
                <img src="./img/P5-Email.png" alt=""/>

                <div class="Email-Container">
                    <p>E-MAIL</p>

                    <div class="Email-Hr"></div>
                    <a href="mailto:tiki-video@gmail.com">tiki-video@gmail.com</a>
                </div>
            </div>
            <div class="Email">
                <img src="./img/P5-Phone.png" alt=""/>

                <div class="Email-Container-2">
                    <p style="text-align: left">Телефон</p>

                    <div class="Email-Hr"></div>
                    <p class="Phones">+38 (095) 486-03-90</p>

                    <p class="Phones">+38 (097) 223-61-16</p>

                </div>
            </div>
            <div class="Social">
                <a class="Vk" href="http://vk.com/public99934402"></a>
                <a class="You" href="https://www.youtube.com/channel/UCCB82heZZLHXeMlntpjfUgg"></a>
                <a class="Facebook" href="https://www.facebook.com/groups/440642142804747/"></a>
            </div>
            <span>Украина, г. Киев</span>
        </div>
        <div class="Contact-Box-2 Data-Form"
             data-mailform-subject="TiKi Video">
            <h3>Напишите нам</h3>

            <div class="Email-Hr"></div>
            <div class="Client-Data">
                <input type="text" id="Name" placeholder="Имя" data-form-field-title="Имя"/>
            </div>
            <div class="Client-Data">
                <input type="text" id="Email" placeholder="E-mail" data-form-field-title="E-mail"/>

            </div>
            <div class="Client-Data">
                <textarea id="textarea" placeholder="Сообщение" data-form-field-title="Сообщение"></textarea>
            </div>
            <div class="Send Data-Form-Button">Отправить</div>

        </div>


    </div>
    <div class="Bottom">

        <div class="Button">Вверх</div>
        <div class="qwe-up"></div>
        <span>tiki-video.com © 2015</span>
    </div>
</div>


<script>
    $('input[type="text"],textarea').placeholder({animate: 'fade'});
</script>
</body>

</html>