(function ($) {


    // UPF-Windows Default Variables

    var Class_Window = 'UPF-Window',
        Class_WindowWrapper = 'UPF-Window-Wrapper',
        Class_WindowContent = 'UPF-Window-Content',
        Class_WindowClose = 'UPF-Window-Close',
        Selector_Window = '.' + Class_Window,
        Selector_WindowWrapper = '.' + Class_WindowWrapper,
        Selector_WindowContent = '.' + Class_WindowContent,
        Selector_WindowClose = '.' + Class_WindowClose;

    // Count Center Position
    var Function_WindowPosition = function () {
        var Height = ( $(window).height() - $(Selector_WindowContent).actual('outerHeight') ) / 2;
        if (Height > 0 && $(Selector_WindowContent).actual('outerHeight') < $(window).height()) {
            $(Selector_WindowContent).css('marginTop', Height);
        } else {
            $(Selector_WindowContent).css('marginTop', '');
        }
    };


    // Methods
    var Methods = {

        // Open - bind event with function "open"

        open: function (Content, UserOptions) {

            var Settings = $.extend({
                'Event': 'click',
                'Duration': 1000
            }, UserOptions);


            $(this).bind(
                Settings['Event'] + Selector_Window, {
                    obj: this,
                    Content: Content,
                    Settings: Settings
                },
                Methods.show);


        },

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Show
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        show: function (e) {
            // Close Prev Window
            $(Selector_Window).hide(e.data.Settings.Duration,
                function () {
                    $(this).remove();
                });


            $('html').css('overflow', 'hidden');
            var Content = '';

            if ($(this).attr('data-window-selector')) {
                // Set By Selector
                Content = $($(this).attr('data-window-selector')).html();

            } else if ($(this).attr('data-window-youtube')) {
                // Set By Selector
                Content = '<iframe width="100%" height="100%" frameborder="0" src="' + $(this).attr('data-window-youtube') + '" allowfullscreen autoplay></iframe>';

            } else {
                // Set By Variable Default
                Content = e.data.Content;

            }

            // Append Content
            $('body').append(
                '<div class="' + Class_Window + '">' +
                '<div class="' + Class_WindowWrapper + '"></div>' +
                '<span class="' + Class_WindowClose + ' Default">×</span>' +
                '<div class="' + Class_WindowContent + '">' +
                Content +
                '</div>' +
                '</div>');

            $(window).resize(Function_WindowPosition);

            // Show Window
            $(Selector_Window).fadeIn({
                start: Function_WindowPosition(),
                duration: e.data.Settings.Duration
            });


            // Close By Click To Wrapper
            $(Selector_WindowWrapper + ',' + Selector_WindowClose).bind(
                e.data.Settings.Event + Selector_Window,
                {
                    Settings: e.data.Settings.Duration
                },
                Methods.close);

            return false;
        },

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Close
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        close: function (e) {
            $(Selector_Window).fadeOut(e.data.Settings.Duration, function () {
                $(Selector_Window).remove();
                $('html').css('overflow', 'auto');
            });
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// End Methods
    };


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $.fn.upf_window = function (Method) {
        if (Methods['method'] == 'close', Methods['method'] == 'show') {
            return Methods[Method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else {
            return Methods.open.apply(this, arguments);
        }
    };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


})(jQuery);


